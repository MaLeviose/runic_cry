﻿using UnityEngine;
using System.Collections;

public class PauseMenu : MonoBehaviour {

	public Texture BackgroundTexture;

	void OnGUI(){
		GUI.DrawTexture (new Rect (0, 0, Screen.width, Screen.height), BackgroundTexture);
	}

}
